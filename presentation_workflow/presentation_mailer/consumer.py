import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    presentation = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        message="The presentation is accepted",
        from_email="admin@conference.go",
        recipient_list=[presentation["presenter_email"]],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    presentation = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        message="The presentation has been rejected: " + presentation["title"],
        from_email="admin@conference.go",
        recipient_list=[presentation["presenter_email"]],
        fail_silently=False,
    )


# whil True:
try:
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    queue_dict = {
        "presentation_approvals": process_approval,
        "presentation_rejections": process_rejection,
    }

    for queue_name, func in queue_dict.items():
        channel.queue_declare(queue=queue_name)
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=func,
            auto_ack=True,
        )

    channel.start_consuming()
except AMQPConnectionError:
    print("AMQPConnectionError")
