import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"

    headers = {"Authorization": PEXELS_API_KEY}
    # Create a dictionary for the headers to use in the request

    params = {"query": f"{city} {state}", "per_page": 1}
    # Create the URL for the request with the city and state

    res = requests.get(url, params=params, headers=headers)
    print(res)
    # Make the request

    pexel_dict = res.json()
    # Parse the JSON response

    # anti corrupting layer
    try:
        picture_url = pexel_dict["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}

    except KeyError:
        return {"picture_url": None}
    # Return a dictionary that contains a `picture_url` key and
    # one of the URLs for one of the pictures in the response


def get_lat_lon(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"

    params = {"q": f"{city},{state},USA", "appid": OPEN_WEATHER_API_KEY}
    # Create the URL for the geocoding API with the city and state

    res = requests.get(url, params=params)
    # Make the request

    res_json = res.json()
    # Parse the JSON response

    lat = res_json[0]["lat"]
    lon = res_json[0]["lon"]
    return lat, lon
    # Get the latitude and longitude from the response


def get_weather_data(city, state):
    lat, lon = get_lat_lon(city, state)

    url = "http://api.openweathermap.org/data/2.5/weather"
    # Create the URL for the geocoding API with the city and state

    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # Get the main temperature and the weather's description and put
    #   them in a dictionary

    res = requests.get(url, params=params)
    # Make the request

    res_json = res.json()
    # Parse the JSON response

    return {
        "description": res_json["weather"][0]["description"],
        "temp": res_json["main"]["temp"],
    }  # Return the dictionary
